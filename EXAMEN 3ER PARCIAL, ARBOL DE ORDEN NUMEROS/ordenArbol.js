class orden_ARBOL {
    constructor() {
        this.Padre = "";
        this.nodos = [];

    }

    NPadre(Nombre, Posicion, Nivel, hI, hD, Valor, Padre) {
        var NOD = new orden_NODO(Nombre, Posicion, Nivel, hI, hD, Valor, Padre)
        this.nodos.push(NOD)
        return NOD;
    }


    NewNodo(Valor, Nombre) {
        let nodo = new orden_NODO(Nombre, null, null, 0, 0, Valor, null)
        if (this.nodos.length == 1) {
            nodo.Padre = this.Padre;
            nodo.Nivel = this.Padre.Nivel + 1;
            if (nodo.Valor < this.Padre.Valor)
                nodo.Posicion = 'hI';
            else
                nodo.Posicion = 'hD';
            this.nodos.push(nodo)
        }


        else if(this.nodos.length == 2)
        {
            nodo.Padre = this.Padre;
            nodo.Nivel = this.Padre.Nivel + 1;
            if (nodo.Valor < this.Padre.Valor)
                nodo.Posicion = 'hI';
            else
                nodo.Posicion = 'hD';
            this.nodos.push(nodo)
        }


        else if(this.nodos.length == 3)
        {
            nodo.Padre = this.Padre;
            nodo.Nivel = this.Padre.Nivel + 1;
            if (nodo.Valor < this.Padre.Valor)
                nodo.Posicion = 'hI';
            else
                nodo.Posicion = 'hD';
            this.nodos.push(nodo)
        }


        else if(this.nodos.length == 4)
        {
            nodo.Padre = this.Padre;
            nodo.Nivel = this.Padre.Nivel + 1;
            if (nodo.Valor < this.Padre.Valor)
                nodo.Posicion = 'hI';
            else
                nodo.Posicion = 'hD';
            this.nodos.push(nodo)
        }
    }
}
//                                ____
//                              _(____)_
//                       ___ooO_(_o__o_)_Ooo___
// 	                      BRYAN HERNANDEZ SOLIS