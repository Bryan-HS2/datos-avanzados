var family =
    {
        "Persona1":
            {
                "Nombre": "BRYAN",
                "Apellido": "HERNANDEZ",
                "Gustos":
                    {
                        "Comida": "POZOLE",
                        "Videojuegos": "SHOTERS",
                        "Musica": "ROCK/METAL",
                        "Peliculas": "ACCION",
                        "Bebida": "CAFE"

                    }
            },
        "Persona2":
            {
                "Nombre": "BELLDANDY",
                "Apellido": "HERNANDEZ",
                "Gustos":
                    {
                        "Dulces": "CHOCOLATES/PAPITAS",
                        "Bebida": "AGUA",
                        "Musica": "RAP/REGAETON",
                        "Peliculas": "TERROR7SUSPENSO",
                        "Literatura": "NOVELAS"

                    }
            }

    }
console.log(family);